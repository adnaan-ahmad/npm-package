import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, View, Text, Alert, Button, Image, Modal, Platform } from 'react-native';
import * as Facebook from 'expo-facebook';
import * as WebBrowser from 'expo-web-browser';
import * as Google from 'expo-auth-session/providers/google';
import { GoogleAuthProvider, signInWithCredential } from 'firebase/auth';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import { WebView } from 'react-native-webview'








module.exports.PaymentGateway = (props) => {
  const [paymentdialog, setpaymentdialog] = useState(true)

  return (
    props.type === 'Razorpay' ?
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0
      }}>
        <Modal
          style={{}}
          isVisible={paymentdialog}
          animationIn="slideInLeft"
          animationOut="slideOutRight">
          <View style={{ flex: 1, backgroundColor: "white", paddingVertical: 0 }}>
            <WebView
              useWebKit={true}
              style={{ width: '100%', height: '100%' }}
              originWhitelist={['*']}
              automaticallyAdjustContentInsets={false}
              scrollEnabled={false}
              javaScriptEnabled={true}
              thirdPartyCookiesEnabled={true}
              androidLayerType={"hardware"}
              mixedContentMode="always"
              domStorageEnabled={true}
              allowFileAccess={true}
              javaScriptCanOpenWindowsAutomatically={true}
              cacheEnabled={true}
              allowUniversalAccessFromFileURLs={true}
              ignoreSslError={true}
              onNavigationStateChange={(state) => {
                // alert(state.url);
                if (state.url.includes("status=close")) {
                  setpaymentdialog(false);
                }
                else if (state.url.includes("status=authorized")) {
                  // alert("Payment Completed");
                  showMessage({
                    message: "Order Completed",
                    type: "success",
                    duration: 6000
                  });
                  dispatch(storeaddcartloader(true));
                  setTimeout(function () {
                    setpaymentdialog(false);
                    setTimeout(function () {
                      dispatch(storecheckoutdisable({}));
                      dispatch(storeproductsincart({ items: [], discount: 0, total_payable: 0 }));

                    }, 1200);
                  }, 1000);
                }
                else if (state.url.includes("status=failed")) {
                  showMessage({
                    message: "Order Placement Failed",
                    type: "danger",
                    duration: 6000
                  });
                  setpaymentdialog(false);
                }
              }}
              onMessage={(text) => {
                var data = JSON.parse(text.nativeEvent.data);
                alert(data.type);
              }}
              source={{
                html: `<!DOCTYPE html>
                                          <html>
                                            <head>
                                              <meta http-equiv="Access-Control-Allow-Origin" content="*">
                                              <meta charset="utf-8">
                                              <title>My first three.js app</title>
                                              <style>
                                              </style>
                                            </head>
                                            <body>
                                              <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
                                              <script>
                                              // alert("hii");
                                                setTimeout(function(){
                                                    window.start_payment = function(amount,email,mobile_number,name,color){
                                                      
                                                  var options = {
                                                          description: 'Order Payment',
                                                          image: 'https://i.imgur.com/3g7nmJC.png',
                                                          currency: 'INR',
                                                          key: 'rzp_test_YHA1kx3mizdPXa', // Your api key
                                                          amount: Math.round(amount * 100),
                                                          name: 'StyBizz',
                                                          redirect: true,
                                                          modal: {
                                                            ondismiss: function(){
                                                                 window.location = "//status=close";
                                                             }
                                                          },
                                                          prefill: {
                                                              email: email,
                                                              contact: mobile_number,
                                                              name: name
                                                          },
                                                          
                                                      theme: {color: color}
                                                      }
                                                      var rzp1 = new Razorpay(options);
                                                      rzp1.open();
                                                    }
                                                    
    
                                                    window.parent.start_payment("`+ props.totalPayable + `","` + props.email + `","` + props.mobileNumber + `","` + props.name + `","` + props.color + `");
    
    
                                              }, 200);
                                              </script>
                                            </body>
                                          </html>`}}
          
            />
          
          </View>
        </Modal>
      </View> : <></>
  );

}














Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

module.exports.Notification = () => {
  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log(response);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  return expoPushToken

}


async function sendPushNotification(expoPushToken) {
  const message = {
    to: expoPushToken,
    sound: 'default',
    title: 'Original Title123',
    body: 'And here is the body123',
    data: { someData: 'goes here' },
  };

  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Device.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}








WebBrowser.maybeCompleteAuthSession();

module.exports.Google = (props) => {

  const firebaseConfig = {
    apiKey: props.apiKey,
    authDomain: props.authDomain,
    projectId: props.projectId,
    storageBucket: props.storageBucket,
    messagingSenderId: props.messagingSenderId,
    appId: props.appId
  };

  let Firebase;

  Firebase = firebase.initializeApp(firebaseConfig);

  const auth = Firebase.auth()

  const [request, response, promptAsync] = Google.useIdTokenAuthRequest(
    {
      clientId: props.clientId,
    },
  );

  useEffect(() => {
    if (response?.type === 'success') {
      const { id_token } = response.params;
      const credential = GoogleAuthProvider.credential(id_token);
      const credentialPromise = signInWithCredential(auth, credential);
      credentialPromise.then((credential) => {
        console.log(credential);
      })
    }
  }, [response]);

  return (
    <View style={{}}>

      <Button
        disabled={!request}
        title="Login"
        onPress={() => {
          promptAsync();
        }}
      />
    </View>
  );
}





module.exports.Facebook = (props) => {

  const loginWithFacebook = async () => {
    try {
      await Facebook.initializeAsync({
        appId: props.appId,
      });


      const { type, token } =
        await Facebook.logInWithReadPermissionsAsync({
          permissions: ['public_profile', 'email'],
        });

      if (type === 'success') {
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
        Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
      } else {

      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
      console.log(message)
    }
  }

  return (
    <View style={{ marginTop: 30 }}>
      <Text
        
        onPress={() => loginWithFacebook()}
        style={{}}>Login</Text>
    </View>
  )

}
