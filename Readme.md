# Google SignIn
[Google SignIn](https://docs.expo.dev/versions/v45.0.0/sdk/auth-session/)
```sh
<Google
    clientId = clientId
    apiKey = apiKey
    authDomain = authDomain
    projectId = projectId
    storageBucket= storageBucket
    messagingSenderId = messagingSenderId
    appId = appId
/>
```

# Facebook SignIn
[Facebook SignIn](https://docs.expo.dev/versions/latest/sdk/facebook/)
```sh
<Facebook
    appId = appId
/>
```

# Push Notification

```sh
<Notification />
```

# Razorpay Payment Gateway

```sh
<PaymentGateway
    type = 'Razorpay'
    totalPayable = {Number}
    email = email
    mobileNumber = {mobileNumber}
    name = name
    color = color
/>
```